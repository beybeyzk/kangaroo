<p align="center">
    <a href="https://www.datatable.online/?from=github" target="_blank">
        <img src="images/kangaroo.svg" width="300" alt="Kangaroo Logo">
    </a>
</p>

# Kangaroo 
Kangaroo is a SQL client and admin tool for popular databases(SQLite / MySQL / PostgreSQL / ...) on Windows / MacOS / Linux, support table design, query, model, sync, export/import etc, focus on comfortable, fun and developer friendly.

__Read this in other languages:__ English | [中文(Chinese)](./README.md)

## Official website / 官方网站
[English](https://www.datatable.online/?from=gitee) | [中文(Chinese)](https://www.datatable.online/zh/?from=gitee)

## Support database
Database support capability level: __Planned__ / __Partial__ / __Full(:100:)__

| Database    | Version | Query     | Editing   | Designer  | Export    | Import    | Hint      | Modeling | DB Sync |
|-------------|---------|-----------|-----------|-----------|-----------|-----------|-----------|----------|---------|
| SQLite      | 3.0 +   | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | in progress | in progress |
| MySQL       | 5.5 +   | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | in progress | in progress |
| MariaDB     | 10.0 +  | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | in progress | in progress |
| PostgreSQL  | 9.0 +   | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | in progress | in progress |
| Redis       |         | Planned   | Planned   | Planned   | Planned   | Planned   | Planned   | Planned  | Planned |
| Oracle      |         |           |           |           |           |           |           |          |         |
| SQL Server  |         |           |           |           |           |           |           |          |         |

**Hint**: Code intellisense or Code autocomplete


## Release
Development version keep one version(vNext) only from year 2023, App version and its packages will be updated regularly(one or two weeks), Beta and Stable version depend on test result and stabilization.

| Architect | Windows         | MacOS           | Linux           | iOS             | Android         | Harmony         |
|-----------|-----------------|-----------------|-----------------|-----------------|-----------------|-----------------|
| x86-64 | [v2.99.2](https://www.datatable.online/en/download/v2.99.2.230428?from=github&os=windows) | [v2.99.2](https://www.datatable.online/en/download/v2.99.2.230428?from=github&os=macos) | [v2.99.2](https://www.datatable.online/en/download/v2.99.2.230428?from=github&os=linux) |
| ARM64 | | | | | | |
| RISCV64 | | | | | | |

## Support the Project
If you like Kangaroo and you want to support its development, you could __scan QR code__ to donate via PayPal / Wechat / Alipay.

[![Support project](./images/pay_wide.png)](https://www.datatable.online/?from=github "Support project")

## Screenshots
[![Start page of connection](./images/kangaroo-start.png)](https://www.datatable.online/?from=github "Start page of connection")
[![Object explorer](./images/kangaroo-explorer.png)](https://www.datatable.online/?from=github "Object explorer")
[![Object search](./images/kangaroo-search.png)](https://www.datatable.online/?from=github "Object search")
[![Kangaroo split view](./images/kangaroo-workspace.png)](https://www.datatable.online/?from=github "Kangaroo split view")
[![Kangaroo grid view in table with custom columns](./images/kangaroo-grid.png)](https://www.datatable.online/?from=github "Kangaroo grid view in table with custom columns")
[![Kangaroo grid view in table with where statement](./images/kangaroo-grid2.png)](https://www.datatable.online/?from=github "Kangaroo grid view in table with where statement")
[![Kangaroo grid view in form](./images/kangaroo-form.png)](https://www.datatable.online/?from=github "Kangaroo grid view in form")
[![Kangaroo query view](./images/kangaroo-query.png)](https://www.datatable.online/?from=github "Kangaroo query view")
[![Kangaroo schema designer form](./images/kangaroo-designer.png)](https://www.datatable.online/?from=github "Kangaroo schema designer form")
[![Kangaroo view designer](./images/kangaroo-view.png)](https://www.datatable.online/?from=github "Kangaroo view designer")
[![Kangaroo function designer](./images/kangaroo-function.png)](https://www.datatable.online/?from=github "Kangaroo function designer")
[![Kangaroo visual builder](./images/kangaroo-sql-builder.png)](https://www.datatable.online/?from=github "Kangaroo visual builder")
[![Kangaroo export assistant](./images/kangaroo-export.png)](https://www.datatable.online/?from=github "Kangaroo export assistant")
[![Kangaroo import assistant](./images/kangaroo-import.png)](https://www.datatable.online/?from=github "Kangaroo import assistant")
[![Kangaroo setting dialog with dark theme](./images/kangaroo-setting.png)](https://www.datatable.online/?from=github "Kangaroo setting dialog with dark theme")
[![Kangaroo shortcut setting dialog](./images/kangaroo-shortcut.png)](https://www.datatable.online/?from=github "Kangaroo shortcut setting dialog")
